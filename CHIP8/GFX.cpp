#include <cstdlib>
#include <cstring>
#include <iostream>
#include "GFX.h"
namespace CHIP8 {

	GFX::GFX(int pixelWidth, int pixelHeight) {
		updated = true;
		std::memset(pixel, 0, 64 * 32 * sizeof(bool));
		int y = 0, x = 0;
		for (; y < 32; ++y) {
			for (x = 0; x < 64; ++x) {
				pixels[y * 64 + x] = { x*pixelWidth,y*pixelHeight,pixelWidth,pixelHeight };
			}
		}
	}

	bool GFX::getPixel(int x, int y) {
		return pixel[y * 64 + x];
	}

	bool GFX::setPixel(int x, int y, bool p) {
		bool tmp = false, pix = false;
		if ((y * 64 + x) < (64 * 32)) {
			tmp = pixel[y * 64 + x];
			pixel[y * 64 + x] ^= p;
			pix = pixel[y * 64 + x];
			if (tmp == true && pix == false) {
				return true;
			}
		}
		//std::cout << tmp << "^" << p << "=" << pix << " C?" << (tmp^pix) << " U?" << (updated | (tmp^pix)) << std::endl;
		return false;
	}

	void GFX::clear() {
		std::memset(pixel, 0, 64 * 32 * sizeof(bool));
		updated = true;
	}

	bool GFX::draw(int x, int y, int n, int i, CHIP8::RAM &ram) {
		int row = 0;
		bool flip = false;
		for (; row < n; ++row) {
			int colum = 0;
			for (; colum < 8; ++colum) {
				flip |= setPixel(x + colum, y + row, (((ram.getByte(i + row) >> (7 - colum)) & 0x0001) > 0));
			}
		}
		updated = true;
		return flip;
	}

	void GFX::drawFrame(SDL_Renderer *gRenderer) {
		int x = 0, y = 0;
		for (; y < 32; ++y) {
			for (x = 0; x < 64; ++x) {
				if (pixel[y * 64 + x]) {
					SDL_SetRenderDrawColor(gRenderer, 0xCC, 0xCC, 0xCC, 0xCC);
				} else {
					SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xFF);
				}
				SDL_RenderFillRect(gRenderer, &pixels[y * 64 + x]);
			}
		}
		updated = false;
	}

	bool GFX::isUpdated() {
		return updated;
	}

	std::ostream& operator<<(std::ostream &out, CHIP8::GFX &in) {
		out << "GFX {\n";
		for (int i = 0; i < 32; ++i) {
			for (int j = 0; j < 64; ++j) {
				out << in.getPixel(j, i);
			}
			out << "\n";
		}
		out << "}";
		return out;
	}
}
