/*
 * File:   Motherboard.h
 * Author: diego
 *
 * Created on August 21, 2015, 1:22 PM
 */

#ifndef MOTHERBOARD_H
#define	MOTHERBOARD_H

#include <iostream>
#include <cstring>
#include <SDL.h>
#include "RAM.h"

namespace CHIP8 {
	static unsigned char fontset[] = {
		0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
		0x20, 0x60, 0x20, 0x20, 0x70, // 1
		0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
		0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
		0x90, 0x90, 0xF0, 0x10, 0x10, // 4
		0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
		0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
		0xF0, 0x10, 0x20, 0x40, 0x40, // 7
		0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
		0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
		0xF0, 0x90, 0xF0, 0x90, 0x90, // A
		0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
		0xF0, 0x80, 0x80, 0x80, 0xF0, // C
		0xE0, 0x90, 0x90, 0x90, 0xE0, // D
		0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
		0xF0, 0x80, 0xF0, 0x80, 0x80 // F
	};

	/**CHIP8 motherboard build-in hardware
	 */
	class MotherBoard {
	private:
		unsigned char delayTimer;
		unsigned char soundTimer;
		bool keypad[16];
	protected:


	public:

		MotherBoard() : delayTimer((unsigned short)0u), soundTimer((unsigned short)0u), running(false), powerOFF(false) {
			std::memset(keypad, 0, 16 * sizeof(unsigned char));
		}
		bool running;
		bool powerOFF;
		void decreaseDelayTimer();
		void decreaseSoundTimer();
		void setDelayTimer(char);
		void setSoundTimer(char);
		unsigned char getDelayTimer();
		unsigned char getSoundTimer();
		int setKeypad(int, bool);
		bool getKeypad(int);
		int readInput(SDL_Event*);
		int getFisrtPressed();
		void notifyCycleExecuted();
		bool loadProgram(char*, CHIP8::RAM&);

		friend std::ostream& operator<<(std::ostream &out, MotherBoard &in);
	};
}
#endif	/* MOTHERBOARD_H */

