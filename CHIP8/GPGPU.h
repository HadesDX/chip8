/*
* File:   CPU.h
* Author: diego
*
* Created on August 21, 2015, 1:13 PM
*/

#ifndef GPGPU_H
#define	GPGPU_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <utility>
#include <CL/cl.hpp>

#include "CPU.h"
#include "RAM.h"
#include "GFX.h"
#include "Motherboard.h"

namespace CHIP8 {

	/** CPU CHIP8
	* 35 OPCODES 2 Bytes long.
	* 15 8-bit registers from V0 to VE
	* 16th register "carry flag"
	*/
	class GPGPU {
	private:


	public:

		//GPGPU_H;
		inline void checkErr(cl_int err, const char * name);
		void TEST();
		
		
		friend std::ostream& operator<<(std::ostream &out, CPU &in);
	};
}
#endif	/* GPGPU_H */
