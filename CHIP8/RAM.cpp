#include <cstdlib>
#include <cstring>
#include "RAM.h"
namespace CHIP8 {

	unsigned char RAM::getByte(int index) {
		return memory[index];
	}

	unsigned short RAM::getShort(int index) {
		return memory[index] << 8 | memory[index + 1];
	}

	unsigned char RAM::setByte(int index, unsigned char value) {
		memory[index] = value;
		return 0;
	}

	unsigned short RAM::setShort(int index, unsigned short value) {
		memory[index] = value >> 8;
		memory[index + 1] = (unsigned char)value & 0xFF;
		return 0;
	}

	unsigned short RAM::loadFontSet(unsigned char* font) {
		std::memcpy(memory, font, 80 * sizeof(unsigned char));
		return 0;
	}

	std::ostream& operator<<(std::ostream &out, CHIP8::RAM &in) {
		out << "RAM { RAM:" << 4096 << " }";
		return out;
	}
}
