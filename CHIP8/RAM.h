/* 
 * File:   RAM.h
 * Author: diego
 *
 * Created on August 21, 2015, 1:11 PM
 */

#ifndef RAM_H
#define	RAM_H

#include <iostream>
#include <cstring>
namespace CHIP8 {

    /** RAM CPU CHIP8
     * 4k Memory
     */
    class RAM {
    private:
        unsigned char memory [4096];
    public:

        RAM() {
            std::memset(memory, 0, 4096 * sizeof (unsigned char));
        }
        unsigned char getByte(int);
        unsigned short getShort(int);
        unsigned char setByte(int, unsigned char);
        unsigned short setShort(int, unsigned short);
        unsigned short loadFontSet(unsigned char*);
        friend std::ostream& operator<<(std::ostream &out, RAM &in);
    };
}

#endif	/* RAM_H */

