#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include "Motherboard.h"


namespace CHIP8 {

	void MotherBoard::decreaseDelayTimer() {
		if (delayTimer > 0) {
			--delayTimer;
		} else if (delayTimer == 0) {
			//std::cout << "TIME!\n";
		}
	}

	void MotherBoard::decreaseSoundTimer() {
		if (soundTimer > 0) {
			--soundTimer;
			std::cout << '\a';
			//std::cout << "BEEP!\n";
		}
	}

	void MotherBoard::setDelayTimer(char t) {
		delayTimer = t;
	}

	void MotherBoard::setSoundTimer(char t) {
		soundTimer = t;
	}

	unsigned char MotherBoard::getDelayTimer() {
		return delayTimer;
	}

	unsigned char MotherBoard::getSoundTimer() {
		return soundTimer;
	}

	int MotherBoard::setKeypad(int index, bool value) {
		keypad[index] = value;
		return index;
	}

	bool MotherBoard::getKeypad(int index) {
		return keypad[index];
	}

	int MotherBoard::readInput(SDL_Event *event) {
		SDL_Event e = *event;
		if (e.type == SDL_KEYDOWN) {
			//Select surfaces based on key press
			switch (e.key.keysym.sym) {
			case SDLK_1:
				return setKeypad(1, true);
				break;
			case SDLK_2:
				return setKeypad(2, true);
				break;
			case SDLK_3:
				return setKeypad(3, true);
				break;
			case SDLK_4:
				return setKeypad(12, true);
				break;
			case SDLK_q:
				return setKeypad(4, true);
				break;
			case SDLK_w:
				return setKeypad(5, true);
				break;
			case SDLK_e:
				return setKeypad(6, true);
				break;
			case SDLK_r:
				return setKeypad(13, true);
				break;
			case SDLK_a:
				return setKeypad(7, true);
				break;
			case SDLK_s:
				return setKeypad(8, true);
				break;
			case SDLK_d:
				return setKeypad(9, true);
				break;
			case SDLK_f:
				return setKeypad(14, true);
				break;
			case SDLK_z:
				return setKeypad(10, true);
				break;
			case SDLK_x:
				return setKeypad(0, true);
				break;
			case SDLK_c:
				return setKeypad(11, true);
				break;
			case SDLK_v:
				return setKeypad(15, true);
				break;
			}
		} else if (e.type == SDL_KEYUP) {
			//Select surfaces based on key press
			switch (e.key.keysym.sym) {
			case SDLK_1:
				setKeypad(1, false);
				break;
			case SDLK_2:
				setKeypad(2, false);
				break;
			case SDLK_3:
				setKeypad(3, false);
				break;
			case SDLK_4:
				setKeypad(12, false);
				break;
			case SDLK_q:
				setKeypad(4, false);
				break;
			case SDLK_w:
				setKeypad(5, false);
				break;
			case SDLK_e:
				setKeypad(6, false);
				break;
			case SDLK_r:
				setKeypad(13, false);
				break;
			case SDLK_a:
				setKeypad(7, false);
				break;
			case SDLK_s:
				setKeypad(8, false);
				break;
			case SDLK_d:
				setKeypad(9, false);
				break;
			case SDLK_f:
				setKeypad(14, false);
				break;
			case SDLK_z:
				setKeypad(10, false);
				break;
			case SDLK_x:
				setKeypad(0, false);
				break;
			case SDLK_c:
				setKeypad(11, false);
				break;
			case SDLK_v:
				setKeypad(15, false);
				break;
			}
		}
		return -1;
	}

	int MotherBoard::getFisrtPressed() {
		for (unsigned int i = 0; i < 16; ++i) {
			if (keypad[i]) {
				return i;
			}
		}
		return -1;
	}

	void MotherBoard::notifyCycleExecuted() {
		decreaseDelayTimer();
		decreaseSoundTimer();
	}

	bool MotherBoard::loadProgram(char *fileName, CHIP8::RAM& ram) {
		bool r = true;
		std::ifstream file;
		file.open(fileName, std::ios::in | std::ios::binary);
		if (!file.is_open()) {
			std::cout << "Could not open file" << std::endl;
			return false;
		}
		file.seekg(0, std::ios::end);
		std::streamoff end = file.tellg();
		file.seekg(0, std::ios::beg);
		std::streamoff begin = file.tellg();
		long int size = (long)(end - begin);
		char* buff = new char[size];
		file.read(buff, size);
		if (file.gcount() != size) {
			std::cout << "An error has ocurred reading file" << std::endl;
			std::cout << "File reading: " << file.gcount() << "/" << size << std::endl;
			r = false;
		} else {
			int index = 0x200;
			int i = 0;
			for (; i < size; ++i, ++index) {
				ram.setByte(index, buff[i]);
			}
		}
		std::cout << "File reading: " << file.gcount() << "/" << size << std::endl;
		file.close();
		delete buff;
		return r;
	}

	std::ostream& operator<<(std::ostream &out, CHIP8::MotherBoard &in) {
		out << "Motherboard { DelayTimer:" << static_cast<int> (in.getDelayTimer()) << ", SoundTimer:" << static_cast<int> (in.getDelayTimer());
		for (int i = 0; i < 16; i++) {
			out << ", k" << i << ":" << static_cast<int> (in.getKeypad(i));
		}
		out << " }";
		return out;
	}
}
