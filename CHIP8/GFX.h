/* 
 * File:   GFX.h
 * Author: diego
 *
 * Created on August 21, 2015, 1:19 PM
 */

#ifndef GFX_H
#define	GFX_H

#include <iostream>
#include <cstring>
#include <SDL.h>
#include "RAM.h"

namespace CHIP8 {

    /** Video output
     * black&white
     * 64 with
     * 32 heigh
     */
    class GFX {
    private:
        bool pixel[64 * 32];
		SDL_Rect pixels[64 * 32];
		bool updated;

    public:

		GFX(int pixelWidth, int pixelHeight);
        bool getPixel(int x, int y);
        bool setPixel(int x, int y, bool value);
        void clear();
        bool draw(int x, int y, int n, int i, CHIP8::RAM &ram);
		void drawFrame(SDL_Renderer*);
		bool isUpdated();
        friend std::ostream& operator<<(std::ostream &out, GFX &in);
    };
}
#endif	/* GFX_H */

