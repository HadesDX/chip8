/*
 * File:   CPU.h
 * Author: diego
 *
 * Created on August 21, 2015, 1:13 PM
 */

#ifndef CPU_H
#define	CPU_H
#include <iostream>
#include <cstring>
#include <ctime>

#include "RAM.h"
#include "GFX.h"
#include "Motherboard.h"

namespace CHIP8 {

	/** CPU CHIP8
	 * 35 OPCODES 2 Bytes long.
	 * 15 8-bit registers from V0 to VE
	 * 16th register "carry flag"
	 */
	class CPU {
	private:
		unsigned short indexRegister;
		unsigned short programCounter;
		unsigned short opCode;
		unsigned char registerV[16];
		unsigned short stack[16];
		int stackPointer;
		bool wKeydown;

	public:

		CPU() : indexRegister((unsigned short)0u), programCounter((unsigned short)0x200u), opCode((unsigned short)0u), stackPointer(0), wKeydown(false) {
			srand((unsigned int) time(0));
			std::memset(registerV, 0, 16 * sizeof(unsigned char));
			std::memset(stack, 0, 16 * sizeof(unsigned short));
		}

		void fetch(CHIP8::RAM &ram);
		void printDecoded(CHIP8::RAM &ram);
		bool execute(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int *keydown);
		void setIndexRegister(unsigned short);
		void setProgramCounter(unsigned short);
		void setOpCode(unsigned short);
		void setVRegister(int, unsigned char);
		unsigned short getIndexRegister();
		unsigned short getProgramCounter();
		unsigned short getOpCode();
		unsigned char getVRegister(int);
		void pushStack(unsigned short);
		unsigned short popStack();
		bool waitingKeydown();
		void printHEXDEC(int,unsigned int);
		//OPCODES
		bool OP0NNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP00E0(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP00EE(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP1NNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP2NNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP3XNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP4XNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP5XY0(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP6XNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP7XNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP8XY0(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP8XY1(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP8XY2(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP8XY3(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP8XY4(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP8XY5(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP8XY6(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP8XY7(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP8XYE(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OP9XY0(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OPANNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OPBNNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OPCXNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OPDXYN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OPEX9E(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OPEXA1(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OPFX07(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OPFX0A(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OPFX15(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OPFX18(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OPFX1E(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OPFX29(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OPFX33(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OPFX55(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		bool OPFX65(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown);
		friend std::ostream& operator<<(std::ostream &out, CPU &in);
	};
}
#endif	/* CPU_H */

