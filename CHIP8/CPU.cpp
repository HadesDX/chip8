#include <cstdlib>
#include <cstring>
#include <iostream>
#include <iomanip>
#include "CPU.h"
#include "RAM.h"
#include "Motherboard.h"

namespace CHIP8 {

	void CPU::fetch(CHIP8::RAM &ram) {
		opCode = ram.getShort(programCounter);
	}
	void CPU::printDecoded(CHIP8::RAM &ram) {
		using namespace std;
		cout << uppercase;
		cout << "PC:" << std::setw(4) << hex << programCounter << dec << "(" << std::setw(4) << programCounter << ") " << hex << std::setw(4) << std::setfill('0') << opCode << ":" << dec;
		int f = (opCode & 0xF000) >> 12;
		switch (f) {
		case 0:
			if (opCode == 0x00E0) {
				cout << "00E0: CLS\t\t\tCLS";
			} else if (opCode == 0x00EE) {
				cout << "00EE: RET\t\t\tRET " << hex << std::setw(4) << stack[stackPointer - 1] << "(" << dec << std::setw(4) << stack[stackPointer - 1] << ")";
			} else {
				cout << "0nnn: SYS addr:\t\tSYS ";
				printHEXDEC(3, opCode & 0x0FFF);
			}
			break;
		case 1:
			cout << "1nnn: JP addr\t\tJP ";
			printHEXDEC(3, opCode & 0x0FFF);
			break;
		case 2:
			cout << "2nnn: CALL addr\t\tCALL " << hex << std::setw(4) << (opCode & 0x0FFF) << "(" << dec << std::setw(4) << (opCode & 0x0FFF) << ")";
			break;
		case 3:
			cout << "3xkk: SE Vx, byte\t\tSE V" << ((opCode & 0x0F00) >> 8) << " ";
			printHEXDEC(2, registerV[(opCode & 0x0F00) >> 8]);
			cout << ", ";
			printHEXDEC(2, opCode & 0x00FF);
			break;
		case 4:
			cout << "4xkk: SNE Vx, byte\t\tSNE V" << ((opCode & 0x0F00) >> 8) << ", " << hex << (opCode & 0x00FF);
			break;
		case 5:
			if ((opCode & 0x000F) == 0) {
				cout << "5xy0: SE Vx, Vy\t\t\t\tSE V" << ((opCode & 0x0F00) >> 8) << ", V" << ((opCode & 0x00F0) >> 4);
			} else {
				cout << "UNKNOW";
			}
			break;
		case 6:
			cout << "6xkk: LD Vx, byte\t\tLD V" << ((opCode & 0x0F00) >> 8) << ", " << hex << (opCode & 0x00FF);
			break;
		case 7:
			cout << "7xkk: ADD Vx, byte\t\tADD V" << ((opCode & 0x0F00) >> 8) << ", " << hex << (opCode & 0x00FF);
			break;
		case 9:
			if ((opCode & 0x000F) == 0) {
				cout << "9xy0: SNE Vx, Vy\t\t\t\tSNE V" << ((opCode & 0x0F00) >> 8) << ", V" << ((opCode & 0x00F0) >> 4);
			} else {
				cout << "UNKNOW";
			}
			break;
		case 10:
			cout << "Annn: LD I, addr\t\tLD I, " << hex << (opCode & 0x0FFF);
			break;
		case 11:
			cout << "Bnnn: JP V0, addr\t\tJP V0, " << hex << (opCode & 0x0FFF);
			break;
		case 12:
			cout << "Cxkk: RND Vx, byte\t\tRND V" << ((opCode & 0x0F00) >> 8) << ", " << hex << (opCode & 0x00FF);
			break;
		case 13:
			cout << "Dxyn: DRW Vx, Vy, nibble\tDRW V" << ((opCode & 0x0F00) >> 8) << ", V" << ((opCode & 0x00F0) >> 4) << ", " << (opCode & 0x000F);
			break;
		case 8:
		{
			int o = opCode & 0x000F;
			switch (o) {
			case 0:
				cout << "8xy0: LD Vx, Vy\t\tLD V" << ((opCode & 0x0F00) >> 8) << ", V" << ((opCode & 0x00F0) >> 4);
				break;
			case 1:
				cout << "8xy1: OR Vx, Vy\t\tOR V" << ((opCode & 0x0F00) >> 8) << ", V" << ((opCode & 0x00F0) >> 4);
				break;
			case 2:
				cout << "8xy2: AND Vx, Vy\t\tAND V" << ((opCode & 0x0F00) >> 8) << ", V" << ((opCode & 0x00F0) >> 4);
				break;
			case 3:
				cout << "8xy3: XOR Vx, Vy\t\tXOR V" << ((opCode & 0x0F00) >> 8) << ", V" << ((opCode & 0x00F0) >> 4);
				break;
			case 4:
				cout << "8xy4: ADD Vx, Vy\t\tADD V" << ((opCode & 0x0F00) >> 8) << ", V" << ((opCode & 0x00F0) >> 4);
				break;
			case 5:
				cout << "8xy5: SUB Vx, Vy\t\tSUB V" << ((opCode & 0x0F00) >> 8) << ", V" << ((opCode & 0x00F0) >> 4);
				break;
			case 6:
				cout << "8xy6: SHR Vx {, Vy}\tSHR V" << ((opCode & 0x0F00) >> 8) << " {, V" << ((opCode & 0x00F0) >> 4) << "}";
				break;
			case 7:
				cout << "8xy7: SUBN Vx, Vy\tSUBN V" << ((opCode & 0x0F00) >> 8) << ", V" << ((opCode & 0x00F0) >> 4);
				break;
			case 0xE:
				cout << "8xyE: SHL Vx {, Vy}\tSHL V" << ((opCode & 0x0F00) >> 8) << " {, V" << ((opCode & 0x00F0) >> 4) << "}";
				break;
			default:
				cout << "UNKNOW";
			}
		}
		break;
		case 14:
		{
			int o = opCode & 0x00FF;
			switch (o) {
			case 0x9E:
				cout << "Ex9E: SKP Vx\t\tSKP V" << hex << ((opCode & 0x0F00) >> 8);
				break;
			case 0xA1:
				cout << "ExA1: SKNP Vx\t\tSKNP V" << hex << ((opCode & 0x0F00) >> 8);
				break;
			default:
				cout << "UNKNOW";
			}
		}
		break;
		case 15:
		{
			int o = opCode & 0x00FF;
			switch (o) {
			case 0x07:
				cout << "Fx07: LD Vx, DT\t\tLD V" << ((opCode & 0x0F00) >> 8) << ", DT";
				break;
			case 0x0A:
				cout << "Fx0A: LD Vx, K\t\tLD V" << ((opCode & 0x0F00) >> 8) << ", K";
				break;
			case 0x15:
				cout << "Fx15: LD DT, Vx\t\tLD DT, V" << ((opCode & 0x0F00) >> 8);
				break;
			case 0x18:
				cout << "Fx18: LD ST, Vx\t\tLD ST, V" << ((opCode & 0x0F00) >> 8);
				break;
			case 0x1E:
				cout << "Fx1E: ADD I, Vx\t\tADD I, V" << ((opCode & 0x0F00) >> 8);
				break;
			case 0x29:
				cout << "Fx29: LD F, Vx\t\tLD F, V" << ((opCode & 0x0F00) >> 8);
				break;
			case 0x33:
				cout << "Fx33: LD B, Vx\t\tLD B, V" << ((opCode & 0x0F00) >> 8);
				break;
			case 0x55:
				cout << "Fx55 LD [I], Vx\t\tLD [I], V" << ((opCode & 0x0F00) >> 8);
				break;
			case 0x65:
				cout << "Fx65: LD Vx, [I]\t\tLD V" << ((opCode & 0x0F00) >> 8) << ", [I]";
				break;
			default:
				cout << "UNKNOW";
			}
		}
		break;
		default:
			cout << "UNKNOW";
		}
		cout << endl << dec;
	};

	bool CPU::execute(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int *keydown) {

		bool (CHIP8::CPU::*opFunction)(CHIP8::RAM &, CHIP8::GFX &, CHIP8::MotherBoard &, int) = NULL;

		unsigned short op = opCode & 0xF000;
		switch (op) {
		case 0:
			switch (opCode) {
			case 0x00E0://00E0	Clears the screen.
				opFunction = &CPU::OP00E0;
				break;
			case 0x00EE://00EE	Returns from a subroutine.
				opFunction = &CPU::OP00EE;
				break;
			default:
				opFunction = &CPU::OP0NNN;
			}
			break;
		case 0x1000://1NNN	Jumps to address NNN.
			opFunction = &CPU::OP1NNN;
			break;
		case 0x2000: //2NNN	Calls subroutine at NNN.
			opFunction = &CPU::OP2NNN;
			break;
		case 0x3000: //3XNN	Skips the next instruction if VX equals NN.
			opFunction = &CPU::OP3XNN;
			break;
		case 0x4000: //4XNN	Skips the next instruction if VX doesn't equal NN.
			opFunction = &CPU::OP4XNN;
			break;
		case 0x5000: //5XY0	Skips the next instruction if VX equals VY.
			opFunction = &CPU::OP5XY0;
			break;
		case 0x6000: //6XNN	Sets VX to NN.
			opFunction = &CPU::OP6XNN;
			break;
		case 0x7000: //7XNN	Adds NN to VX.
			opFunction = &CPU::OP7XNN;
			break;
		case 0x8000:
		{
			op = opCode & 0x000F;
			switch (op) {
			case 0x0000://8XY0	Sets VX to the value of VY.
				opFunction = &CPU::OP8XY0;
				break;
			case 0x0001://8XY1	Sets VX to VX or VY.
				opFunction = &CPU::OP8XY1;
				break;
			case 0x0002://8XY2	Sets VX to VX and VY.
				opFunction = &CPU::OP8XY2;
				break;
			case 0x0003://8XY3	Sets VX to VX xor VY.
				opFunction = &CPU::OP8XY3;
				break;
			case 0x0004://8XY4	Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't.
				opFunction = &CPU::OP8XY4;
				break;
			case 0x0005://8XY5	VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
				opFunction = &CPU::OP8XY5;
				break;
			case 0x0006://8XY6	Shifts VX right by one. VF is set to the value of the least significant bit of VX before the shift.[2]
				opFunction = &CPU::OP8XY6;
				break;
			case 0x0007://8XY7	Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
				opFunction = &CPU::OP8XY7;
				break;
			case 0x000E://8XYE	Shifts VX left by one. VF is set to the value of the most significant bit of VX before the shift.[2]
				opFunction = &CPU::OP8XYE;
				break;
			}
		}
		break;
		case 0x9000://9XY0	Skips the next instruction if VX doesn't equal VY.
			opFunction = &CPU::OP9XY0;
			break;
		case 0xA000://ANNN	Sets I to the address NNN.
			opFunction = &CPU::OPANNN;
			break;
		case 0xC000:
			opFunction = &CPU::OPCXNN;
			break;
		case 0xD000://DXYN	Sprites stored in memory at location in index register (I),
			//8bits wide. Wraps around the screen. If when drawn, clears a pixel,
			//register VF is set to 1 otherwise it is zero. All drawing is XOR drawing 
			//(i.e. it toggles the screen pixels). Sprites are drawn starting at position 
			//VX, VY. N is the number of 8bit rows that need to be drawn. 
			//If N is greater than 1, second line continues at position VX, VY+1, and so on.
			opFunction = &CPU::OPDXYN;
			break;
		case 0xE000:
			op = opCode & 0xF0FF;
			if (op == 0xE09E) {//EX9E	Skips the next instruction if the key stored in VX is pressed.
				opFunction = &CPU::OPEX9E;
			} else if (op == 0xE0A1) {//EXA1	Skips the next instruction if the key stored in VX isn't pressed.
				opFunction = &CPU::OPEXA1;
			}
		case 0xF000:
			op = opCode & 0xFF;
			switch (op) {
			case 0x07://FX07	Sets VX to the value of the delay timer.
				opFunction = &CPU::OPFX07;
				break;
			case 0x0A://FX0A	A key press is awaited, and then stored in VX
				opFunction = &CPU::OPFX0A;
				break;
			case 0x15://FX15	Sets the delay timer to VX.
				opFunction = &CPU::OPFX15;
				break;
			case 0x18://FX18	Sets the sound timer to VX.
				opFunction = &CPU::OPFX18;
				break;
			case 0x1E://FX1E	Adds VX to I.[3]
				opFunction = &CPU::OPFX1E;
				break;
			case 0x29://FX29	Sets I to the location of the sprite for the character in VX.Characters 0 - F(in hexadecimal) are represented by a 4x5 font.
				opFunction = &CPU::OPFX29;
				break;
			case 0x33://FX33	Stores the Binary - coded decimal representation of VX, with the most significant of three digits at the address in I, the middle digit at I plus 1, and the least significant digit at I plus 2. (In other words, take the decimal representation of VX, place the hundreds digit in memory at location in I, the tens digit at location I + 1, and the ones digit at location I + 2.)
				opFunction = &CPU::OPFX33;
				break;
			case 0x55://FX55	Stores V0 to VX in memory starting at address I.[4]
				opFunction = &CPU::OPFX55;
				break;
			case 0x65://FX65	Fills V0 to VX with values from memory starting at address I.[4]
				opFunction = &CPU::OPFX65;
				break;
			}
			break;
		default:
			return false;
		}
		if (opFunction) {
			bool eOK = (this->*opFunction)(ram, gfx, mobo, *keydown);
			if (opCode & 0xF00A) {
				*keydown = -1;
			}
			if (!eOK) {
				return eOK;
			}
			programCounter += 2;
			return true;
		}
		return false;
	}

	void CPU::setIndexRegister(unsigned short ir) {
		indexRegister = ir;
	}

	void CPU::setProgramCounter(unsigned short pc) {
		programCounter = pc;
	}

	void CPU::setOpCode(unsigned short op) {
		opCode = op;
	}

	void CPU::setVRegister(int index, unsigned char value) {
		if (index < 0 || index > 15) {
			throw "Unknown register";
		}
		registerV[index] = value;
	}

	unsigned short CPU::getIndexRegister() {
		return indexRegister;
	}

	unsigned short CPU::getProgramCounter() {
		return programCounter;
	}

	unsigned short CPU::getOpCode() {
		return opCode;
	}

	unsigned char CPU::getVRegister(int index) {
		if (index < 0 || index > 15) {
			throw "Unknown register";
		}
		return registerV[index];
	}

	void CPU::pushStack(unsigned short value) {
		if (stackPointer >= 15) {
			throw "Stack overflow";
		}
		stack[stackPointer] = value;
		++stackPointer;
	}

	unsigned short CPU::popStack() {
		if (stackPointer <= 0) {
			throw "Stack is empty";
		} else if (stackPointer > 15) {
			throw "Non existent stack pointer";
		}
		--stackPointer;
		return stack[stackPointer];
	}
	bool CPU::waitingKeydown() {
		return wKeydown;
	}
	void CPU::printHEXDEC(int w, unsigned int v) {
		std::cout << std::hex << std::setfill('0') << std::setw(w) << v << "(" << std::dec << std::setfill('0') << std::setw(w + 1) << v << ")";
	}
	bool CPU::OP0NNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		std::cout << "0NNN !! IGNORED ";
		printHEXDEC(4, opCode);
		std::cout << "@" << programCounter << std::endl; //0NNN  Calls RCA 1802 program at address NNN. Not necessary for most ROMs.
		return true;
	}
	bool CPU::OP00E0(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		gfx.clear();
		return true;
	}
	bool CPU::OP00EE(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		programCounter = popStack();
		return true;
	}
	bool CPU::OP1NNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		programCounter = (opCode & 0x0FFF) - 2;
		return true;
	}
	bool CPU::OP2NNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		pushStack(programCounter);
		programCounter = (opCode & 0xFFF) - 2;
		return true;
	}
	bool CPU::OP3XNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		if (registerV[(opCode & 0x0F00) >> 8] == (opCode & 0x00FF)) {
			programCounter += 2;
		}
		return true;
	}
	bool CPU::OP4XNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		if (registerV[(opCode & 0x0F00) >> 8] != (opCode & 0x00FF)) {
			programCounter += 2;
		}
		return true;
	}
	bool CPU::OP5XY0(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		if (registerV[(opCode & 0x0F00) >> 8] == registerV[(opCode & 0x00F0) >> 4]) {
			programCounter += 2;
		}
		return true;
	}
	bool CPU::OP6XNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		registerV[(opCode & 0x0F00) >> 8] = opCode & 0x00FF;
		return true;
	}
	bool CPU::OP7XNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		//std::cout << "resgister:" << ((opCode & 0x0F00) >> 8) << " value:" << (int)registerV[(opCode & 0x0F00) >> 8] << " + " << (opCode & 0x00FF) << "=" << (registerV[(opCode & 0x0F00) >> 8] + (opCode & 0x00FF)) << " %256= " << ((registerV[(opCode & 0x0F00) >> 8] + (opCode & 0x00FF)) % 256) << " & 0x00FF " << (((registerV[(opCode & 0x0F00) >> 8] + (opCode & 0x00FF)) % 256) & 0x00FF) << std::endl;
		registerV[(opCode & 0x0F00) >> 8] = ((registerV[(opCode & 0x0F00) >> 8] + (opCode & 0x00FF)) % 256) & 0x00FF;
		return true;
	}
	bool CPU::OP8XY0(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		registerV[(opCode & 0x0F00) >> 8] = registerV[(opCode & 0x00F0) >> 4];
		return true;
	}
	bool CPU::OP8XY1(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		registerV[(opCode & 0x0F00) >> 8] |= registerV[(opCode & 0x00F0) >> 4];
		return true;
	}
	bool CPU::OP8XY2(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		registerV[(opCode & 0x0F00) >> 8] &= registerV[(opCode & 0x00F0) >> 4];
		return true;
	}
	bool CPU::OP8XY3(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		registerV[(opCode & 0x0F00) >> 8] ^= registerV[(opCode & 0x00F0) >> 4];
		return true;
	}
	bool CPU::OP8XY4(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		int vy = registerV[(opCode & 0x00F0) >> 4];
		int vx = registerV[(opCode & 0x0F00) >> 8];
		registerV[(opCode & 0x0F00) >> 8] = ((vx + vy) % 256) & 0xFF;
		if ((vx + vy) > 0x00FF) {
			registerV[15] = 1;
		} else {
			registerV[15] = 0;
		}
		return true;
	}
	bool CPU::OP8XY5(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		int vy = registerV[(opCode & 0x00F0) >> 4];
		int vx = registerV[(opCode & 0x0F00) >> 8];
		registerV[(opCode & 0x0F00) >> 8] = ((vx - vy) % 256) & 0xFF;
		if (vx > vy) {
			registerV[15] = 1;
		} else {
			registerV[15] = 0;
		}
		return true;
	}
	bool CPU::OP8XY6(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		registerV[15] = registerV[(opCode & 0x0F00) >> 8] & 0x1;
		registerV[(opCode & 0x0F00) >> 8] >>= 1;
		return true;
	}
	bool CPU::OP8XY7(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		int vy = registerV[(opCode & 0x00F0) >> 4];
		int vx = registerV[(opCode & 0x0F00) >> 8];
		registerV[(opCode & 0x0F00) >> 8] = ((vy - vx) % 256) & 0xFF;
		if (vx > vy) {
			registerV[15] = 0;
		} else {
			registerV[15] = 1;
		}
		return true;
	}
	bool CPU::OP8XYE(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		registerV[15] = (registerV[(opCode & 0x0F00) >> 8] >> 7) & 0x1;
		registerV[(opCode & 0x0F00) >> 8] <<= 1;
		return true;
	}
	bool CPU::OP9XY0(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		if (registerV[(opCode & 0x0F00) >> 8] != registerV[(opCode & 0x00F0) >> 4]) {
			programCounter += 2;
		}
		return true;
	}
	bool CPU::OPANNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		indexRegister = opCode & 0x0FFF;
		return true;
	}
	bool CPU::OPBNNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		programCounter = (opCode & 0x0FFF) + registerV[0] - 2;
		return true;
	}
	bool CPU::OPCXNN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		unsigned short rnd = (unsigned short)(((double)rand() / ((double)RAND_MAX + 1.0)) * 256);
		registerV[(opCode & 0x0F00) >> 8] = ((opCode & 0xFF) & rnd) & 0xFF;
		return true;
	}
	bool CPU::OPDXYN(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		if (gfx.draw(registerV[(opCode & 0x0F00) >> 8], registerV[(opCode & 0x00F0) >> 4], (opCode & 0x000F), indexRegister, ram)) {
			registerV[15] = 1;
		} else {
			registerV[15] = 0;
		}
		return true;
	}
	bool CPU::OPEX9E(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		if (mobo.getKeypad(registerV[(opCode & 0x0F00) >> 8])) {
			programCounter += 2;
		}
		return true;
	}
	bool CPU::OPEXA1(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		if (!mobo.getKeypad(registerV[(opCode & 0x0F00) >> 8])) {
			programCounter += 2;
		}
		return true;
	}
	bool CPU::OPFX07(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		registerV[(opCode & 0x0F00) >> 8] = mobo.getDelayTimer();
		return true;
	}
	bool CPU::OPFX0A(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		if (keydown == -1) {
			wKeydown = true;
			programCounter -= 2;
		} else {
			wKeydown = false;
			registerV[(opCode & 0x0F00) >> 8] = keydown & 0xF;
		}
		return true;
	}
	bool CPU::OPFX15(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		mobo.setDelayTimer(registerV[(opCode & 0x0F00) >> 8]);
		return true;
	}
	bool CPU::OPFX18(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		mobo.setSoundTimer(registerV[(opCode & 0x0F00) >> 8]);
		return true;
	}
	bool CPU::OPFX1E(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		if (indexRegister + registerV[(opCode & 0x0F00) >> 8] > 0xFFF) {	// VF is set to 1 when range overflow (I+VX>0xFFF), and 0 when there isn't.
			registerV[15] = 1;
		} else {
			registerV[15] = 0;
		}
		indexRegister += registerV[(opCode & 0x0F00) >> 8];
		indexRegister &= 0x0FFF;
		return true;
	}
	bool CPU::OPFX29(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		indexRegister = registerV[(opCode & 0x0F00) >> 8] * 5;
		return true;
	}
	bool CPU::OPFX33(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		unsigned char d = registerV[(opCode & 0xF00) >> 8];
		ram.setByte(indexRegister, d / 100);
		ram.setByte(indexRegister + 1, (d / 10) % 10);
		ram.setByte(indexRegister + 2, (d % 100) % 10);
		return true;
	}
	bool CPU::OPFX55(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		int x = (opCode & 0x0F00) >> 8;
		int i;
		for (i = 0; i <= x; ++i) {
			ram.setByte(indexRegister + i, registerV[i]);
		}
		//indexRegister += x + 1;
		return true;
	}
	bool CPU::OPFX65(CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo, int keydown) {
		int x = (opCode & 0x0F00) >> 8;
		int i;
		for (i = 0; i <= x; ++i) {
			registerV[i] = ram.getByte(indexRegister + i);
		}
		//indexRegister += x + 1;
		return true;
	}

	std::ostream& operator<<(std::ostream &out, CHIP8::CPU &in) {
		out << "CPU {\nPC:" << in.getProgramCounter() << " IR:" << in.getIndexRegister() << " OP:" << std::hex << in.getOpCode() << "\n";
		for (int i = 0; i < 16; i += 4) {
			out << "V";
			in.printHEXDEC(1, i);
			out << ":";
			in.printHEXDEC(2, in.getVRegister(i));
			out << "\t";
			out << "V";
			in.printHEXDEC(1, i + 1);
			out << ":";
			in.printHEXDEC(2, in.getVRegister(i + 1));
			out << "\t";
			out << "V";
			in.printHEXDEC(2, i + 2);
			out << ":";
			in.printHEXDEC(2, in.getVRegister(i + 2));
			out << "\t";
			out << "V";
			in.printHEXDEC(2, i + 3);
			out << ":";
			in.printHEXDEC(2, in.getVRegister(i + 3));
			out << "\n";
		}
		out << "SP:" << in.stackPointer << "\n";
		for (int i = 0; i < in.stackPointer; ++i) {
			out << "S" << i << ":";
			in.printHEXDEC(1, in.stack[i]);
			std::cout << std::endl;
		}
		out << "}";
		return out;
	}

}
