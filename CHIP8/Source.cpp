#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iterator>
#include <iomanip>
#include <chrono>
#include "GPGPU.h" // LLamar la libreria de Opencl antes q la de sdl, Agregar dll cuando es debug para q no le falten archivos al correr.
#include <SDL.h>
#include <thread>
#undef main // Evita un error que marca dos funciones no encontradas
#include "CPU.h"
#include "RAM.h"
#include "GFX.h"
#include "Motherboard.h"


//Screen dimension constants
const int PIXEL_WIDTH = 10;
const int PIXEL_HEIGHT = 10;
const int SCREEN_WIDTH = 64 * PIXEL_WIDTH;
const int SCREEN_HEIGHT = 32 * PIXEL_HEIGHT;
const long long GFX_REFRESH_PER_SECOND = 32;
const int CPU_INST_PER_FRAME = 25;

//Loop vars 
//Main loop flag
bool quit = false;

//Event handler
SDL_Event e;

void test(CHIP8::CPU &c, CHIP8::RAM &ram, CHIP8::GFX &gfx, CHIP8::MotherBoard &mobo) {
	std::cout << "TEST*********************************" << std::endl;
	//c.setVRegister(0, 0);
	//c.setVRegister(1, 0);
	//c.setVRegister(2, 4);
	//c.setVRegister(3, 0);
	//c.setVRegister(4, 4);
	//c.setVRegister(5, 5);
	//c.setVRegister(6, 8);
	//c.setVRegister(7, 10);
	//c.setVRegister(1, 0);
	ram.setShort(0x200, 0xA000);
	ram.setShort(0x202, 0xD015);
	ram.setShort(0x204, 0xA005);
	ram.setShort(0x206, 0xD235);
	//ram.setShort(0x208, 0xA05A);
	//ram.setShort(0x20A, 0xD455);
	//ram.setShort(0x20C, 0xA05F);
	//ram.setShort(0x20E, 0xD675);
	//gfx.setPixel(0, 0, true);
	//gfx.setPixel(63, 0, true);
	//gfx.setPixel(0, 31, true);
	//gfx.setPixel(63, 31, true);
	//gfx.setPixel(64, 10, true);

	//ram.setShort(0x208, 0x5000);

	for (int i = 0; i < 10; ++i) {
		c.fetch(ram);
		c.printDecoded(ram);
		std::cout << std::endl << c << std::endl;
		c.execute(ram, gfx, mobo, 0);
	}
	std::cout << gfx;
	std::cout << "TEST*********************************" << std::endl;
}

void timerEvent(CHIP8::MotherBoard* m) {
	//std::chrono::time_point<std::chrono::steady_clock> *lastTick = new std::chrono::time_point<std::chrono::steady_clock>();
	//std::chrono::time_point<std::chrono::steady_clock> *newTick = new std::chrono::time_point<std::chrono::steady_clock>();
	//long long timeSpan;
	//*lastTick = std::chrono::high_resolution_clock::now();
	//*newTick = std::chrono::high_resolution_clock::now();
	//timeSpan = std::chrono::duration_cast<std::chrono::milliseconds>(*newTick - *lastTick).count();
	while (!m->powerOFF) {
		if (!m->running) {
			m->notifyCycleExecuted();
		}
		//std::cout << "T" << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(16));
	}
}
int main(int argc, char* args[]) {
	CHIP8::GPGPU cl;
	cl.TEST();

	//The window we'll be rendering to
	SDL_Window* window = NULL;

	//The surface contained by the window
	SDL_Surface* screenSurface = NULL;

	//The window renderer
	SDL_Renderer* gRenderer = NULL;

	//Timing
	std::chrono::time_point<std::chrono::steady_clock> *lastTick = new std::chrono::time_point<std::chrono::steady_clock>();
	std::chrono::time_point<std::chrono::steady_clock> *newTick = new std::chrono::time_point<std::chrono::steady_clock>();
	long long timeSpan;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	} else {
		//Create window
		window = SDL_CreateWindow("CHIP8", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (window == NULL) {
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		} else {
			//Get window surface
			gRenderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
			if (gRenderer == NULL) {
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
			}
			//screenSurface = SDL_GetWindowSurface(window);

			//Fill the surface white
			//SDL_FillRect(screenSurface, NULL, SDL_MapRGB(screenSurface->format, 0x00, 0x00, 0x00));

			SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xFF);
			SDL_RenderClear(gRenderer);

			//Setup emu
			using namespace std;
			CHIP8::MotherBoard m;
			CHIP8::CPU c;
			CHIP8::RAM r;
			CHIP8::GFX g = { PIXEL_WIDTH,PIXEL_HEIGHT };

			//cout << g << endl;
			cout << m << endl;
			cout << c << endl;
			cout << r << endl;

			r.loadFontSet(CHIP8::fontset);
			//test(c, r, g, m);

			bool load = false;

			//load = m.loadProgram("ROMS/Chip-8 Demos/Maze (alt) [David Winter, 199x].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Demos/Maze [David Winter, 199x].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Demos/Particle Demo [zeroZshadow, 2008].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Demos/Sierpinski [Sergey Naydenov, 2010].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Demos/Stars [Sergey Naydenov, 2010].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Demos/Trip8 Demo (2008) [Revival Studios].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Demos/Zero Demo [zeroZshadow, 2007].ch8", r);

			//load = m.loadProgram("ROMS/Chip-8 Programs/BMP Viewer - Hello (C8 example) [Hap, 2005].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Programs/Chip8 emulator Logo [Garstyciuks].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Programs/Chip8 Picture.ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Programs/Clock Program [Bill Fisher, 1981].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Programs/Delay Timer Test [Matthew Mikolay, 2010].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Programs/Division Test [Sergey Naydenov, 2010].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Programs/Fishie [Hap, 2005].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Programs/Framed MK1 [GV Samways, 1980].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Programs/Framed MK2 [GV Samways, 1980].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Programs/IBM Logo.ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Programs/Jumping X and O [Harry Kleinberg, 1977].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Programs/Keypad Test [Hap, 2006].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Programs/Life [GV Samways, 1980].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Programs/Minimal game [Revival Studios, 2007].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Programs/Random Number Test [Matthew Mikolay, 2010].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Programs/SQRT Test [Sergey Naydenov, 2010].ch8", r);

			//load = m.loadProgram("ROMS/Chip-8 Games/15 Puzzle [Roger Ivie].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Games/Addition Problems [Paul C. Moews].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Games/Animal Race [Brian Astle].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Games/Pong (1 player).ch8", r);			
			//load = m.loadProgram("ROMS/Chip-8 Games/Space Invaders [David Winter].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Games/Breakout [Carmelo Cortez, 1979].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Games/Brix [Andreas Gustafsson, 1990].ch8", r);
			load = m.loadProgram("ROMS/Chip-8 Games/Blinky [Hans Christian Egeberg, 1991].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Games/Blitz [David Winter].ch8", r);
			//load = m.loadProgram("ROMS/Chip-8 Games/Guess [David Winter].ch8", r);
			//load = m.loadProgram("ROMS/Space Invaders v0.9 (19xx)(David Winter)(Chip-8).bin", r);

			std::thread t1(timerEvent, &m);
			if (!load) {
				m.running = true;
			}

			*lastTick = std::chrono::high_resolution_clock::now();

			while (!quit) {
				*newTick = std::chrono::high_resolution_clock::now();
				int keydown = -1;
				while (SDL_PollEvent(&e) != 0) {
					//User requests quit
					if (e.type == SDL_QUIT) {
						std::cout << "Terminando emulacion." << std::endl;
						m.running = false;
						quit = true;
					}
					if (keydown == -1) {
						keydown = m.readInput(&e);
					} else {
						m.readInput(&e);
					}
					if ((e.key.type == SDL_KEYDOWN) && (e.key.keysym.sym == SDLK_p)) {
						m.running = !m.running;
					}
				}
				keydown = m.getFisrtPressed();

				timeSpan = std::chrono::duration_cast<std::chrono::milliseconds>(*newTick - *lastTick).count();
				if (timeSpan >= 1000 / GFX_REFRESH_PER_SECOND) {
					*lastTick = std::chrono::high_resolution_clock::now();
					if (!m.running) {
						for (int i = 0; i < CPU_INST_PER_FRAME; ++i) {
							//Execute
							c.fetch(r);
							//c.printDecoded(r);
							if (!c.execute(r, g, m, &keydown)) {
								cout << g << endl;
								cout << m << endl;
								cout << c << endl;
								cout << r << endl;
								cout << "PC:" << c.getProgramCounter() << "\t" << hex << c.getOpCode() << ":" << dec;
								c.printDecoded(r);
								cout << endl;
								m.running = true;
							}
						}
					}
					if (g.isUpdated()) {
						//		Draw
						g.drawFrame(gRenderer);
						SDL_RenderPresent(gRenderer);
					}
				}
			}
			m.powerOFF = true;
			t1.join();
		}
	}


	//Destroy window
	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(window);

	//Wait to quit
	SDL_Delay(2000);

	//Quit SDL subsystems
	SDL_Quit();


	return 0;
}